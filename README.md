# TicTacToeVB
![TicTacToeVB](./screenshots/Tictactoevb.jpg) 

## Description
This is a version of the game Tic-Tac-Toe (for more information on the game itself, see [Wikipedia](http://en.wikipedia.org/wiki/Tic-tac-toe)).  This program supports both online and offline game play, and allows for 0 - 2 human players (meaning you can play against another person or the computer, or have a computer play against itself or another computer).

**License:** [GPL v.3](http://www.gnu.org/licenses/gpl-3.0.html)

## Instructions
You may download the setup application from the below link.  Extract both files from the .zip file, then double click on the setup.exe file to install.  Once the program is installed, you will have "Tic Tac Toe" icons on your desktop and in the Games folder on your Start menu.  Click on one of these icons to start the game.

Once the game has loaded, you will see a screen similar to the one at the top of this page.  Select File -> New Game or press Control-N to start a new game.  You will now see a window like the one below.

![TicTacToeVB](./screenshots/Tictactoevbnewgame.jpg) 

For Player 1, you have three choices -- Human Player, Computer (Easy), and Computer (Hard).  Select one of these options, depending on if you want to play the game yourself or have the computer play for you, and then enter a name for this player.

Do the same for Player 2.  Note that for Player 2, you have an additional option -- Remote Player.  Select this option only if you're planning on playing against another user over a local network or the Internet.  If you select this last option, you will see two additional buttons -- Connect to Game and Start New Game.

![TicTacToeVB](./screenshots/Tictactoevbnewonlinegame.jpg) 

If you're playing an offline game, press Ok.  The computer will automatically determine who should go first, and display the current players name in the bottom of the window.  If a computer player is chosen to go first, the computer will automatically take it's turn, then the name at the bottom will change for the next turn.  If it's your turn, simply single-click on the square you wish to take for your turn.

If you're playing an online game, one player must click "Start New Game".  When this button is clicked, a new window will be displayed, showing that computers local network name and internet IP address.  Once that window has loaded, the second player may then click "Connect to Game" and then enter in the address of the first computer to establish the connection.  Once the connection is established, the software will automatically determine who should go first, and display that player's name at the bottom of both windows.  If that player is computer controlled, it will automatically take it's turn.  If it's a human player, then that user must click on the space they wish to take for their turn.

![TicTacToeVB](./screenshots/Tictactoevbconnection.jpg) 

When the game is over, the program will automatically display a message at the bottom of the window along with the name of who won, or a message that the game was a tie.  If you wish to play a new game, you can now press File -> Play Again or Control-P and a new game will start with the existing settings.

When playing an online game, one or both player's may need to modify their firewall settings, to allow the game to connect properly.  Both computers must allow the Tic Tac Toe software to communicate over port 1234, and the player who selects "Start New Game" must allow the program to act as a server and accept connections on port 1234.  Please consult your firewall's documentation to determine how to set this up, if necessary.

## History
**Version 1.0.0:**
> - Initial Release.
> - Released on 24 October 2011.
>
> Download: [Version 1.0.0 Setup](/uploads/782a9f9fdc3b8943737453095cdd2ad0/TicTacToeVBSetup100.zip) | [Version 1.0.0 Source](/uploads/9045773276aea946286b8dd3b7e50bf6/TicTacToeVBSource100.zip)

