﻿' Project Name:  TicTacToeVB
' Copyright 2011 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 02 October 2011

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Imports System.Net.Sockets

Public Class classCommunication
    Const intPortNumber As Integer = 1234 ' The port number to communicate over.
    Public tcpClientSocket As System.Net.Sockets.TcpClient

    ''' <summary>
    ''' Stores a read only boolean value indicating if it is connected.
    ''' </summary>
    Public ReadOnly Property Connected
        Get
            ' Make sure tcpClientSocket has been initialized before returning
            '   it's connected variable.
            '   If it hasn't been, automatically return false.
            If Not IsNothing(tcpClientSocket) Then
                Return tcpClientSocket.Connected
            Else
                Return False
            End If
        End Get
    End Property ' END Connected

    ''' <summary>
    ''' Disconnect from a remote computer.
    ''' </summary>
    Sub Disconnect()
        Try
            ' Make sure tcpClientSocket is initialized and connected before disconnecting.
            If Not IsNothing(tcpClientSocket) Then
                If tcpClientSocket.Connected Then
                    ' If there's an open connection, send a disconnect string to let the 
                    '   remote user know to close the connection.
                    Me.SendData("DISCONNECT")
                End If

                ' Disconnect and dispose the tcpClientSocket object.
                tcpClientSocket.Close()
                tcpClientSocket = Nothing
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Tic Tac Toe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub ' END Disconnect

    ''' <summary>
    ''' Listen for a remote computer to request a connection.
    ''' </summary>
    ''' <returns>Returns true if the connection was successfully established, otherwise false.</returns>
    Function WaitForConnection() As Boolean
        tcpClientSocket = New TcpClient
        Dim tcpListenerServer As New TcpListener(System.Net.IPAddress.Any, intPortNumber)
        tcpListenerServer.Start()
        Try
            tcpClientSocket = tcpListenerServer.AcceptTcpClient()
            Return True
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Tic Tac Toe", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try
    End Function ' END WaitForConnection

    ''' <summary>
    ''' Request a connection with a remote computer.
    ''' </summary>
    ''' <param name="txtIPAddress">IP Address or Host Name of the computer to connect to.</param>
    ''' <returns>Returns True if the connection was successful, False if it wasn't.</returns>
    Function EstablishConnection(ByVal txtIPAddress As String) As Boolean
        Try
            tcpClientSocket = New TcpClient
            tcpClientSocket.Connect(txtIPAddress, intPortNumber)
            Return True
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Tic Tac Toe", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try
    End Function ' END EstablishConnection

    ''' <summary>
    ''' Send a string to a remote computer.
    ''' </summary>
    ''' <param name="strData">Data to send.</param>
    Sub SendData(ByVal strData As String)
        Try
            ' Make sure tcpClientSocket is initialized and connected before sending data.
            If Not IsNothing(tcpClientSocket) Then
                If tcpClientSocket.Connected Then

                    ' Send strData to the remote system.
                    '   Append a "$" character to the end of the string, so we can split multiple 
                    '   up multiple SendData strings in case they arrive at once.
                    Dim networkStream As System.Net.Sockets.NetworkStream = tcpClientSocket.GetStream()
                    Dim bytesTo As Byte() = System.Text.Encoding.ASCII.GetBytes(strData & "$")
                    networkStream.Write(bytesTo, 0, bytesTo.Length)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Tic Tac Toe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub ' END SendData

    ''' <summary>
    ''' Attempt to receive data from a remote computer.
    ''' If nothing is available to receive, return an empty string.
    ''' </summary>
    ''' <returns>Returns a string containing the received data, or an empty string if there was nothing available.</returns>
    Function ReceiveData() As String
        Try
            Dim networkStream As System.Net.Sockets.NetworkStream = tcpClientSocket.GetStream()
            Dim bytesFrom(10024) As Byte
            Dim dataFromClient As String

            ' Make sure tcpClientSocket is initialized and connected.
            If Not IsNothing(tcpClientSocket) Then
                If tcpClientSocket.Connected Then

                    ' If there is data available to read, retrieve it and return it.
                    If tcpClientSocket.Available Then
                        networkStream.Read(bytesFrom, 0, CInt(tcpClientSocket.ReceiveBufferSize))
                        dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom)
                        If dataFromClient.Trim <> "" Then
                            Return dataFromClient.Trim
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Tic Tac Toe", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Return ""
    End Function ' END ReceiveData
End Class ' END classCommunication
