﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Tic Tac Toe")> 
<Assembly: AssemblyDescription("TicTacToe the Game")> 
<Assembly: AssemblyCompany("Cavaliere.Me")> 
<Assembly: AssemblyProduct("Tic Tac Toe")> 
<Assembly: AssemblyCopyright("Copyright 2011 Eric Cavaliere")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("77ff0971-8c31-4a68-b3f6-6f2b302a3481")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 

<Assembly: NeutralResourcesLanguageAttribute("en-US")> 