﻿' Project Name:  TicTacToeVB
' Copyright 2011 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 02 October 2011

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class formDisconnectConfirmation

    Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        ' If the user clicks Cancel, return cancel and close the window.
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub buttonDisconnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonDisconnect.Click
        ' If the user clicks Disconnect, return Yes and close the window.
        Me.DialogResult = System.Windows.Forms.DialogResult.Yes
        Me.Close()
    End Sub
End Class