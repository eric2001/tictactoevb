﻿' Project Name:  TicTacToeVB
' Copyright 2011 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 02 October 2011

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class formMain
    Dim Player1 As classPlayer
    Dim Player2 As classPlayer
    Dim gameCurrent As classGame
    Dim Player2DataReader As New classCommunication

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        TakeSpace(sender)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        TakeSpace(sender)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        TakeSpace(sender)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        TakeSpace(sender)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        TakeSpace(sender)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        TakeSpace(sender)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        TakeSpace(sender)
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        TakeSpace(sender)
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        TakeSpace(sender)
    End Sub

    Private Sub NewGameToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewGameToolStripMenuItem.Click
        ' When starting a new game, first make sure that any existing internet connections are
        '   closed before proceeding.
        If Player2DataReader.Connected Then
            Dim windowDisconnectConfirmation As New formDisconnectConfirmation
            If windowDisconnectConfirmation.ShowDialog = Windows.Forms.DialogResult.Yes Then
                Player2DataReader.Disconnect()
                DisconnectToolStripMenuItem.Enabled = False
            Else
                Exit Sub
            End If
        End If

        ' Get player name and type information from the user, and set up the board for the game.
        gameCurrent = New classGame(panelGameBoard)
        If gameCurrent.NewGame(Player1, Player2, Player2DataReader) = True Then
            If Player2.AI = classPlayer.PlayerAI.Remote Then
                DisconnectToolStripMenuItem.Enabled = True
                timerNetworkReader.Enabled = True
            End If
            PlayAgainToolStripMenuItem.Enabled = True

            ' Start playing the game.
            PlayGame()
        End If
    End Sub

    Private Sub PlayAgainToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PlayAgainToolStripMenuItem.Click
        ' Start another game with the current settings and reset the game board.
        gameCurrent.StartGame(Player1, Player2)

        ' If this is an internet game, figure out who goes first, and tell the other computer.
        If Player2DataReader.Connected Then
            Dim FirstPlayer As Integer
            If Player1.Piece = gameCurrent.playerCurrent.Piece Then
                FirstPlayer = 1
            Else
                FirstPlayer = 2
            End If
            Player2DataReader.SendData("NewGame|" & FirstPlayer.ToString)
        End If

        ' Start playing the game.
        PlayGame()
    End Sub

    ''' <summary>
    ''' Determine if the game is over.
    ''' </summary>
    ''' <returns>Returns true if the game is over, otherwise false.</returns>
    Function IsGameOver() As Boolean
        ' Call CheckForWin to determine if the game has ended.
        Dim intGameStatus As Integer = gameCurrent.CheckForWin

        ' If we get disconnected from Player 2, then the game is over.
        '   Disable the board and return true.
        If Player2.AI = classPlayer.PlayerAI.Remote And Not Player2DataReader.Connected Then
            panelGameBoard.Enabled = False
            lblGameStatus.Text = "Game Over -- Disconnected!"
            Return True
        End If

        ' If the game is over, set the status label accordingly and return true.
        If intGameStatus <> False Then
            panelGameBoard.Enabled = False
            If intGameStatus = 3 Then
                lblGameStatus.Text = "Game Over -- Tie Game!"
            ElseIf intGameStatus = Player1.PieceCode Then
                lblGameStatus.Text = "Game Over -- " & Player1.Name & " Wins!"
            Else
                lblGameStatus.Text = "Game Over -- " & Player2.Name & " Wins!"
            End If
            Return True
        End If

        ' If the game isn't over, return false.
        Return False
    End Function

    ''' <summary>
    ''' Start playing a new game.
    ''' </summary>
    Sub PlayGame()

        ' Reset the board and the status label for the new game.
        gameCurrent.ResetBoard()
        lblGameStatus.Text = "It is " & gameCurrent.playerCurrent.Name & "'s Turn"

        ' While the current player is not a human, disable the board and loop.
        panelGameBoard.Enabled = False

        Do While (gameCurrent.playerCurrent.AI <> classPlayer.PlayerAI.Human)

            ' If the current player is the computer, take a turn, then swap the players.
            If gameCurrent.playerCurrent.AI <> classPlayer.PlayerAI.Remote Then
                Dim intGamePiece As Integer = gameCurrent.playerCurrent.TakeTurn(panelGameBoard)
                If Player2DataReader.Connected And intGamePiece >= 0 Then
                    Player2DataReader.SendData("Button|" & intGamePiece)
                End If
                gameCurrent.SwapPlayers()
                lblGameStatus.Text = "It is " & gameCurrent.playerCurrent.Name & "'s Turn"
            End If

            ' Exit the loop if the game is over.
            If Me.IsGameOver() Then
                Exit Do
            End If
            Application.DoEvents()
        Loop

        ' If the game is not over, re-enable the board so the human can take their turn.
        If Not Me.IsGameOver() Then
            panelGameBoard.Enabled = True
        End If
    End Sub

    ''' <summary>
    ''' Handles playing a turn for the Human user.
    ''' </summary>
    ''' <param name="sender">Reference to the button the user clicked on.</param>
    ''' <remarks></remarks>
    Sub TakeSpace(ByVal sender As System.Object)
        ' Set the clicked button's text to the user's game piece and disable it.
        sender.Text = gameCurrent.playerCurrent.Piece
        sender.Enabled = False

        ' Inform the remote system of the selection, if connected to one.
        If Player2DataReader.Connected Then
            Player2DataReader.SendData("Button|" & sender.tag.ToString)
        End If

        ' If the game is not over yet, swap players
        If Not Me.IsGameOver() Then
            gameCurrent.SwapPlayers()
            lblGameStatus.Text = "It is " & gameCurrent.playerCurrent.Name & "'s Turn"
            Application.DoEvents()

            ' If the other player is a computer or remote player, disable the board.
            panelGameBoard.Enabled = False

            ' If the other player is a remote player, wait for them to take their turn.
            If (gameCurrent.playerCurrent.AI = classPlayer.PlayerAI.Remote) Then
                Do While (gameCurrent.playerCurrent.AI = classPlayer.PlayerAI.Remote) And (Not Me.IsGameOver())
                    Application.DoEvents()
                Loop

                ' If the other player is a computer player, let the computer take it's turn,
                '   then swap the players.
            ElseIf (gameCurrent.playerCurrent.AI <> classPlayer.PlayerAI.Human) Then
                Dim intGamePiece As Integer = gameCurrent.playerCurrent.TakeTurn(panelGameBoard)
                If Player2DataReader.Connected And intGamePiece >= 0 Then
                    Player2DataReader.SendData("Button|" & intGamePiece)
                End If
                gameCurrent.SwapPlayers()
                lblGameStatus.Text = "It is " & gameCurrent.playerCurrent.Name & "'s Turn"
                Application.DoEvents()
            End If

            ' Check and see if the game is over, then enable the game board if it is not.
            If Not Me.IsGameOver() Then
                panelGameBoard.Enabled = True
            End If
        End If
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        ' Exit the application.
        Application.Exit()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutToolStripMenuItem.Click
        ' Show the about dialog.
        Dim windowAbout As New FormAbout
        windowAbout.ShowDialog()
    End Sub

    Private Sub DisconnectToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DisconnectToolStripMenuItem.Click
        ' Manually disconnect from a remote player and end the game.
        If Not IsNothing(Player2DataReader) Then
            Player2DataReader.Disconnect()
        End If
        DisconnectToolStripMenuItem.Enabled = False
        Me.IsGameOver()
    End Sub

    Private Sub formMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ' Make sure to disconnect from an online game before exiting.
        If Not IsNothing(Player2DataReader) Then
            Player2DataReader.Disconnect()
        End If
    End Sub

    Private Sub formMain_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        ' When resizing the form, individually resize and reposition each of the 9 buttons that 
        '   make up the game board.

        ' When the board is minimized, it's width gets really small.
        '   When this happens, don't do anything.
        If Me.Size.Width < Me.MinimumSize.Width Then
            Exit Sub
        End If

        ' Calculate the new width and hight for each button.
        Dim intNewWidth As Integer = (panelGameBoard.Width - 32) / 3
        Dim intNewHeight As Integer = (panelGameBoard.Height - 32) / 3

        ' Loop through each button and set their new width, height and font size.
        Dim oneButton As Button
        For Each oneButton In panelGameBoard.Controls
            oneButton.Width = intNewWidth
            oneButton.Height = intNewHeight
            oneButton.Font = New Font(System.Drawing.FontFamily.GenericSansSerif, Convert.ToSingle(intNewHeight / 2), FontStyle.Bold)
        Next

        ' Loop through each button and reposition them evenly on the board.
        Dim intCounter As Integer
        Dim intStartX As Integer = 8
        Dim intStartY As Integer = 8
        For intCounter = 0 To 6 Step 3
            panelGameBoard.Controls(intCounter).Location = New System.Drawing.Point(intStartX, intStartY)
            panelGameBoard.Controls(intCounter + 1).Location = New System.Drawing.Point(panelGameBoard.Controls(intCounter).Location.X + panelGameBoard.Controls(intCounter).Width + 8, intStartY)
            panelGameBoard.Controls(intCounter + 2).Location = New System.Drawing.Point(panelGameBoard.Controls(intCounter + 1).Location.X + panelGameBoard.Controls(intCounter + 1).Width + 8, intStartY)
            intStartY = panelGameBoard.Controls(intCounter).Location.Y + panelGameBoard.Controls(intCounter).Height + 8
        Next intCounter
    End Sub

    Private Sub timerNetworkReader_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timerNetworkReader.Tick
        ' Handles receiving and processing data from the remote computer for online games.

        ' Disable the timer to make sure we don't end up with multiple ticks happening at the same time.
        timerNetworkReader.Enabled = False

        ' If the user is no longer connected to the remote player, exit the sub, leaving the timer disabled.
        If Not Player2DataReader.Connected Then
            Exit Sub
        End If

        ' Attempt to receive data from the remote system.
        Dim strReceivedData As String = Player2DataReader.ReceiveData()

        ' If anything was available, split the string up into individual commands and process each one.
        If strReceivedData <> "" Then
            Dim strOneCommand As String
            For Each strOneCommand In strReceivedData.Split("$")

                ' If the remote user sent a disconnect string, disconnect on this side and end the game.
                If strOneCommand.StartsWith("DISCONNECT") Then
                    Player2DataReader.Disconnect()
                    DisconnectToolStripMenuItem.Enabled = False
                    Me.IsGameOver()
                    Exit Sub
                End If

                ' If the user sent a new game string, set up playerCurrent and playerNext, then
                '   start playing the new game.
                If strOneCommand.StartsWith("NewGame|") Then
                    Dim intFirst As Integer = strOneCommand.Substring(strOneCommand.IndexOf("|") + 1)
                    If intFirst = 1 Then
                        gameCurrent.playerCurrent = Player2
                        gameCurrent.playerNext = Player1
                    Else
                        gameCurrent.playerCurrent = Player1
                        gameCurrent.playerNext = Player2
                    End If
                    timerNetworkReader.Enabled = True
                    PlayGame()
                    Exit Sub
                End If

                ' If the user sent a button string, process their turn, then check to see if the game is over.
                '  If the game has ended, exit this sub with the timer disabled, or else swap players.
                If strOneCommand.StartsWith("Button|") Then
                    Dim intPosition As Integer = strOneCommand.Substring(strOneCommand.IndexOf("|") + 1)
                    panelGameBoard.Controls(intPosition).Text = gameCurrent.playerCurrent.Piece
                    panelGameBoard.Controls(intPosition).Enabled = False
                    If Me.IsGameOver() Then
                        timerNetworkReader.Enabled = True
                        Exit Sub
                    End If
                    gameCurrent.SwapPlayers()
                    lblGameStatus.Text = "It is " & gameCurrent.playerCurrent.Name & "'s Turn"
                    Application.DoEvents()
                End If
            Next
        End If

        ' The game is still going on -- re-enable the timer before exiting.
        timerNetworkReader.Enabled = True
    End Sub
End Class
