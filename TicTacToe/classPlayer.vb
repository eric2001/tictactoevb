﻿' Project Name:  TicTacToeVB
' Copyright 2011 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 02 October 2011

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class classPlayer
    ''' <summary>
    ''' Represents one Space on the game board.
    ''' </summary>
    Private Class BoardSpace
        ''' <summary>
        ''' Stores a value representing the instances position on the board from 0 to 8.
        ''' </summary>
        ''' <remarks>0 for UL, 1 for UC, 2 for UR, 3 for ML, 4 for MC, 5 for MR,
        ''' 6 for LL, 7 for LC, 8 for LR.</remarks>
        Public intIndex As Integer

        ''' <summary>
        ''' Stores a value from 0 to 5 representing the rank of the board space.  The higher the value the more important the space.
        ''' </summary>
        ''' <remarks>0 represents a space that's already taken. 1, 2, 3 represent empty spaces.
        ''' 4 indicates an empty space where an opponent already has two in a row and 
        ''' 5 indicates an empty space where this player already has two in a row.</remarks>
        Public intRank As Integer
    End Class ' END BoardSpace

    ''' <summary>
    ''' Indicates if the player is a Person (Human), Computer (Easy or Hard) or Internet Player (Remote).
    ''' </summary>
    Enum PlayerAI
        Human = 0
        Easy = 1
        Hard = 2
        Remote = 3
    End Enum ' END  PlayerAI.

    ''' <summary>
    ''' Represents the available game pieces (X or O).
    ''' </summary>
    Enum GamePiece
        X = 1
        O = 2
    End Enum ' END GamePiece.

    ''' <summary>
    ''' Stores a value indicating the current player's name.
    ''' </summary>
    ''' <value>A String containing the Player's Name.</value>
    ''' <returns>a String containing the Player's Name.</returns>
    ''' <remarks>Normally this is set from the classPlayer New function, except when receiving a name over the internet.</remarks>
    Public Property Name As String
        Get
            Return PlayerName
        End Get
        Set(ByVal value As String)
            PlayerName = value
        End Set
    End Property ' END Name.

    ''' <summary>
    ''' Returns a variable of type PlayerAI indicating the player's AI setting (or Human / Remote if not a computer player).
    ''' </summary>
    Public ReadOnly Property AI As PlayerAI
        Get
            Return PlayerType
        End Get
    End Property ' END AI.

    ''' <summary>
    ''' Returns a variable of type PieceCode indicating the player's game piece.
    ''' </summary>
    Public ReadOnly Property PieceCode As GamePiece
        Get
            Return PlayerPiece
        End Get
    End Property ' END PieceCode.

    ''' <summary>
    ''' Returns a string containing the player's game piece (X or O).
    ''' </summary>
    Public ReadOnly Property Piece As String
        Get
            Return PlayerPiece.ToString
        End Get
    End Property ' END Piece.

    Dim PlayerType As PlayerAI
    Dim PlayerName As String
    Dim PlayerPiece As GamePiece

    ''' <summary>
    ''' Create a new Player.
    ''' </summary>
    ''' <param name="newName">A string containing the player's name</param>
    ''' <param name="newType">An integer or PlayerAI value indicating the type of player (0 Human, 1 Computer Easy, 2 Computer Hard, 3 Remote).</param>
    ''' <param name="newPiece">An integer or GamePiece value indicating the player's game piece (1 for X, 2 for O).</param>
    Sub New(ByVal newName As String, ByVal newType As PlayerAI, ByVal newPiece As GamePiece)
        PlayerType = newType
        PlayerName = newName
        PlayerPiece = newPiece
    End Sub ' END New.

    ''' <summary>
    ''' Ranks the game board.  Used for the Hard level AI.
    ''' </summary>
    ''' <param name="panelGameBoard">A variable of type Panel containing the current game board.</param>
    ''' <returns>Returns a list of BoardSpace's with the current ranking of each space.</returns>
    Private Function RankGameBoardHard(ByRef panelGameBoard As Panel) As List(Of BoardSpace)
        Dim arrSpaces As New List(Of BoardSpace)
        ' Default space ranks for an empty board.
        ' Result is:
        '2|1|2
        '-----
        '1|3|1
        '-----
        '2|1|2
        Dim intCounter As Integer
        Dim boolSwitch As Boolean = True
        For intCounter = 0 To 8
            Dim intBoardSpace As New BoardSpace
            intBoardSpace.intIndex = intCounter
            If boolSwitch = True Then
                intBoardSpace.intRank = 2
            Else
                intBoardSpace.intRank = 1
            End If
            arrSpaces.Add(intBoardSpace)
            boolSwitch = (boolSwitch = False)
        Next
        arrSpaces(4).intRank = 3

        ' set 0 for taken spaces.
        For intCounter = 0 To 8
            If panelGameBoard.Controls(intCounter).Text <> "" Then
                arrSpaces(intCounter).intRank = 0
            End If
        Next

        ' Look for places where either player is one turn away from winning.
        '  Rank this player as a 5 and the opponent as a 4 for any places where this is the case.
        Dim intMyTakenSpaces As Integer = 0
        Dim intLastEmptySpace As Integer = 0
        Dim intOpponentTakenSpaces As Integer = 0

        ' Horizontal Wins.
        For intCounter = 0 To 6 Step 3
            Dim intSecondCounter As Integer
            intMyTakenSpaces = 0
            intOpponentTakenSpaces = 0
            intLastEmptySpace = -1
            For intSecondCounter = intCounter To intCounter + 2
                If panelGameBoard.Controls(intSecondCounter).Text = Me.Piece Then
                    intMyTakenSpaces = intMyTakenSpaces + 1
                ElseIf panelGameBoard.Controls(intSecondCounter).Text <> "" Then
                    intOpponentTakenSpaces = intOpponentTakenSpaces + 1
                Else
                    intLastEmptySpace = intSecondCounter
                End If
            Next
            If intMyTakenSpaces = 2 And intLastEmptySpace > -1 Then
                arrSpaces(intLastEmptySpace).intRank = 5
            End If
            If intOpponentTakenSpaces = 2 And intLastEmptySpace > -1 Then
                If arrSpaces(intLastEmptySpace).intRank < 4 Then
                    arrSpaces(intLastEmptySpace).intRank = 4
                End If
            End If
        Next

        ' Look for Vertical Wins
        For intCounter = 0 To 2
            Dim intSecondCounter As Integer
            intMyTakenSpaces = 0
            intOpponentTakenSpaces = 0
            intLastEmptySpace = -1
            For intSecondCounter = intCounter To intCounter + 6 Step 3
                If panelGameBoard.Controls(intSecondCounter).Text = Me.Piece Then
                    intMyTakenSpaces = intMyTakenSpaces + 1
                ElseIf panelGameBoard.Controls(intSecondCounter).Text <> "" Then
                    intOpponentTakenSpaces = intOpponentTakenSpaces + 1
                Else
                    intLastEmptySpace = intSecondCounter
                End If
            Next
            If intMyTakenSpaces = 2 And intLastEmptySpace > -1 Then
                arrSpaces(intLastEmptySpace).intRank = 5
            End If
            If intOpponentTakenSpaces = 2 And intLastEmptySpace > -1 Then
                If arrSpaces(intLastEmptySpace).intRank < 4 Then
                    arrSpaces(intLastEmptySpace).intRank = 4
                End If
            End If
        Next


        ' Check for diagonal wins
        intMyTakenSpaces = 0
        intOpponentTakenSpaces = 0
        intLastEmptySpace = -1
        For intCounter = 0 To 8 Step 4
            If panelGameBoard.Controls(intCounter).Text = Me.Piece Then
                intMyTakenSpaces = intMyTakenSpaces + 1
            ElseIf panelGameBoard.Controls(intCounter).Text <> "" Then
                intOpponentTakenSpaces = intOpponentTakenSpaces + 1
            Else
                intLastEmptySpace = intCounter
            End If
        Next
        If intMyTakenSpaces = 2 And intLastEmptySpace > -1 Then
            arrSpaces(intLastEmptySpace).intRank = 5
        End If
        If intOpponentTakenSpaces = 2 And intLastEmptySpace > -1 Then
            If arrSpaces(intLastEmptySpace).intRank < 4 Then
                arrSpaces(intLastEmptySpace).intRank = 4
            End If
        End If
        intMyTakenSpaces = 0
        intOpponentTakenSpaces = 0
        intLastEmptySpace = -1
        For intCounter = 2 To 6 Step 2
            If panelGameBoard.Controls(intCounter).Text = Me.Piece Then
                intMyTakenSpaces = intMyTakenSpaces + 1
            ElseIf panelGameBoard.Controls(intCounter).Text <> "" Then
                intOpponentTakenSpaces = intOpponentTakenSpaces + 1
            Else
                intLastEmptySpace = intCounter
            End If
        Next
        If intMyTakenSpaces = 2 And intLastEmptySpace > -1 Then
            arrSpaces(intLastEmptySpace).intRank = 5
        End If
        If intOpponentTakenSpaces = 2 And intLastEmptySpace > -1 Then
            If arrSpaces(intLastEmptySpace).intRank < 4 Then
                arrSpaces(intLastEmptySpace).intRank = 4
            End If
        End If

        Return arrSpaces
    End Function ' END RankGameBoardHard.

    ''' <summary>
    ''' Ranks the game board.  Used for the Easy level AI.
    ''' </summary>
    ''' <param name="panelGameBoard">A variable of type Panel containing the current game board.</param>
    ''' <returns>Returns a list of BoardSpace's with the current ranking of each space.</returns>
    ''' <remarks>This function sets everything to either 0 (taken) or 1 (empty).</remarks>
    Private Function RankGameBoardEasy(ByRef panelGameBoard As Panel) As List(Of BoardSpace)
        Dim arrSpaces As New List(Of BoardSpace)

        ' Default space ranks for an empty board -- 1 for everything.
        Dim intCounter As Integer
        For intCounter = 0 To 8
            Dim intBoardSpace As New BoardSpace
            intBoardSpace.intIndex = intCounter
            intBoardSpace.intRank = 1
            arrSpaces.Add(intBoardSpace)
        Next

        ' set 0 for taken spaces.
        For intCounter = 0 To 8
            If panelGameBoard.Controls(intCounter).Text <> "" Then
                arrSpaces(intCounter).intRank = 0
            End If
        Next

        Return arrSpaces
    End Function ' END RankGameBoardEasy

    ''' <summary>
    ''' Handles taking a turn for a computer player.
    ''' </summary>
    ''' <param name="panelGameBoard">Contains a Panel variable representing the current game board.</param>
    ''' <returns>Returns an integer representing the space that was taken, or -1 if no space was taken.</returns>
    Function TakeTurn(ByRef panelGameBoard As Panel) As Integer
        ' Delay for a second to maintain the illusion of thought.
        System.Threading.Thread.Sleep(1000)

        ' Rank the current game board using the appropriate easy or hard function.
        Dim arrSpaces As List(Of BoardSpace)
        If Me.PlayerType = PlayerAI.Easy Then
            arrSpaces = RankGameBoardEasy(panelGameBoard)
        Else
            arrSpaces = RankGameBoardHard(panelGameBoard)
        End If

        ' Figure out the value of the highest ranking space (1 to 5) and then
        '   pick randomly from among all spaces with that rank.
        Dim intCounter As Integer = 5
        Dim intChoice As Integer = -1
        While intCounter > 0
            Dim intHighestRank = From emptyspaces In arrSpaces Where emptyspaces.intRank = intCounter
            If (intHighestRank.Count > 0) Then
                Dim rndChoice = New Random(Now.Millisecond)
                intChoice = intHighestRank(rndChoice.Next(0, intHighestRank.Count)).intIndex
                Exit While
            End If
            intCounter = intCounter - 1
        End While

        ' If a valid space was chosen, take that space and then return it's position.
        If intChoice >= 0 Then
            panelGameBoard.Controls(intChoice).Text = Me.Piece
            panelGameBoard.Controls(intChoice).Enabled = False
        End If
        Return intChoice
    End Function ' END TakeTurn.
End Class ' END classPlayer.
