﻿' Project Name:  TicTacToeVB
' Copyright 2011 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 02 October 2011

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Public Class classGame
    Public playerCurrent, playerNext As classPlayer
    Dim panelGameBoard As Panel

    ''' <summary>
    ''' Create a new Game.
    ''' </summary>
    ''' <param name="GameBoard">A panel representing the game board.</param>
    Sub New(ByVal GameBoard As Panel)
        panelGameBoard = GameBoard
    End Sub ' END New

    ''' <summary>
    ''' Reset the game board.
    ''' </summary>
    Public Sub ResetBoard()
        ' Empty each button's text property, enable it, then enable the game board.
        Dim oneButton As Button
        For Each oneButton In panelGameBoard.Controls
            oneButton.Text = ""
            oneButton.Enabled = True
        Next
        panelGameBoard.Enabled = True
    End Sub ' END ResetBoard

    ''' <summary>
    ''' Swap playerCurrent and playerNext around.
    ''' </summary>
    Public Sub SwapPlayers()
        ' Swap the contents of playerCurrent and playerNext.
        '   Used to change turns.
        Dim playerTemp As classPlayer
        playerTemp = playerCurrent
        playerCurrent = playerNext
        playerNext = playerTemp
    End Sub ' END SwapPlayers

    ''' <summary>
    ''' Check and see if the game is over.
    ''' </summary>
    ''' <returns>Returns 0 (false) if the game is NOT over, otherwise returns 1 if X wins, 2 if O wins, or 3 for a tie game.</returns>
    Public Function CheckForWin() As Integer
        Dim counter As Integer

        ' Look for Horizontal Wins
        For counter = 0 To 6 Step 3
            If (panelGameBoard.Controls(counter).Text = panelGameBoard.Controls(counter + 1).Text) And _
                (panelGameBoard.Controls(counter).Text = panelGameBoard.Controls(counter + 2).Text) And _
                (panelGameBoard.Controls(counter).Text <> "") Then
                If playerCurrent.Piece = panelGameBoard.Controls(counter).Text Then
                    Return Convert.ToInt32(playerCurrent.PieceCode)
                Else
                    Return Convert.ToInt32(playerNext.PieceCode)
                End If
            End If
        Next

        ' Look for Vertical Wins
        For counter = 0 To 2
            If (panelGameBoard.Controls(counter).Text = panelGameBoard.Controls(counter + 3).Text) And _
                (panelGameBoard.Controls(counter).Text = panelGameBoard.Controls(counter + 6).Text) And _
                 (panelGameBoard.Controls(counter).Text <> "") Then
                If playerCurrent.Piece = panelGameBoard.Controls(counter).Text Then
                    Return Convert.ToInt32(playerCurrent.PieceCode)
                Else
                    Return Convert.ToInt32(playerNext.PieceCode)
                End If
            End If
        Next

        ' Check for diagonal wins
        If (panelGameBoard.Controls(0).Text = panelGameBoard.Controls(4).Text) And _
            (panelGameBoard.Controls(8).Text = panelGameBoard.Controls(4).Text) And _
            (panelGameBoard.Controls(4).Text <> "") Then
            If playerCurrent.Piece = panelGameBoard.Controls(4).Text Then
                Return Convert.ToInt32(playerCurrent.PieceCode)
            Else
                Return Convert.ToInt32(playerNext.PieceCode)
            End If
        End If
        If (panelGameBoard.Controls(2).Text = panelGameBoard.Controls(4).Text) And _
            (panelGameBoard.Controls(6).Text = panelGameBoard.Controls(4).Text) And _
            (panelGameBoard.Controls(4).Text <> "") Then
            If playerCurrent.Piece = panelGameBoard.Controls(4).Text Then
                Return Convert.ToInt32(playerCurrent.PieceCode)
            Else
                Return Convert.ToInt32(playerNext.PieceCode)
            End If
        End If

        ' Check for tie game
        For counter = 0 To 8
            If panelGameBoard.Controls(counter).Text = "" Then
                Return False ' there's still at least one empty space, continue playing
            End If
        Next

        ' Game is a tie
        Return 3
    End Function ' END CheckForWin

    ''' <summary>
    ''' Start a new Game.  Returns two instances of classPlayer and and instance of classCommunication by references.
    ''' </summary>
    ''' <param name="playerOne">An object representing the first Player, to be initialized by this function.</param>
    ''' <param name="playerTwo">An object representing the second Player, to be initialized by this function.</param>
    ''' <param name="commRemotePlayer">An instance of classCommunication for interacting with the second player.  To be initialized by this function if necessary.</param>
    ''' <returns>Returns true if successful, false if the new game was aborted at any point.</returns>
    Public Function NewGame(ByRef playerOne As classPlayer, ByRef playerTwo As classPlayer, ByRef commRemotePlayer As classCommunication) As Boolean
        Dim newGameSettings As New formNewGame

        ' Get information from the user on the two players.
        Dim resultNewGame As DialogResult = newGameSettings.ShowDialog()

        If resultNewGame = Windows.Forms.DialogResult.OK Then
            ' Start a new offline game.
            'The X player usually goes first, so we'll assign X to whichever player goes first and O to the other.
            Dim rndCoin As New Random(Now.Millisecond)
            If rndCoin.Next(1, 3) = 1 Then
                playerOne = New classPlayer(newGameSettings.txtPlayer1Name.Text, newGameSettings.comboPlayer1Type.SelectedIndex, classPlayer.GamePiece.X)
                playerTwo = New classPlayer(newGameSettings.txtPlayer2Name.Text, newGameSettings.comboPlayer2Type.SelectedIndex, classPlayer.GamePiece.O)
                playerCurrent = playerOne
                playerNext = playerTwo
            Else
                playerOne = New classPlayer(newGameSettings.txtPlayer1Name.Text, newGameSettings.comboPlayer1Type.SelectedIndex, classPlayer.GamePiece.O)
                playerTwo = New classPlayer(newGameSettings.txtPlayer2Name.Text, newGameSettings.comboPlayer2Type.SelectedIndex, classPlayer.GamePiece.X)
                playerCurrent = playerTwo
                playerNext = playerOne
            End If


        ElseIf resultNewGame = Windows.Forms.DialogResult.Yes Then
            ' Start a new online game, this computer will wait for a connection.
            Dim windowWaitForPlayer2 As New formWaitForConnection
            windowWaitForPlayer2.Show()
            Application.DoEvents()
            If windowWaitForPlayer2.WaitForPlayer(commRemotePlayer) = False Then
                Return False
            End If

            ' Read Opponent's Name
            Dim dataFromClient As String = ""
            Do While dataFromClient = ""
                dataFromClient = commRemotePlayer.ReceiveData()
            Loop
            dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"))

            'The X player usually goes first, so we'll assign X to whichever player goes first and O to the other.
            Dim rndCoin As New Random(Now.Millisecond)
            If rndCoin.Next(1, 3) = 1 Then
                playerOne = New classPlayer(newGameSettings.txtPlayer1Name.Text, newGameSettings.comboPlayer1Type.SelectedIndex, classPlayer.GamePiece.X)
                playerTwo = New classPlayer(dataFromClient, newGameSettings.comboPlayer2Type.SelectedIndex, classPlayer.GamePiece.O)
                playerCurrent = playerOne
                playerNext = playerTwo
            Else
                playerOne = New classPlayer(newGameSettings.txtPlayer1Name.Text, newGameSettings.comboPlayer1Type.SelectedIndex, classPlayer.GamePiece.O)
                playerTwo = New classPlayer(dataFromClient, newGameSettings.comboPlayer2Type.SelectedIndex, classPlayer.GamePiece.X)
                playerCurrent = playerTwo
                playerNext = playerOne
            End If

            ' Send this player's name and piece (X means server goes first, O means client goes first).
            commRemotePlayer.SendData(playerOne.Name & "|" & playerOne.Piece)

        ElseIf resultNewGame = Windows.Forms.DialogResult.No Then
            ' Start a new online game, this computer will establish a connection with the other player.
            Dim windowConnectToOpponent As New FormNetworkConnection
            If windowConnectToOpponent.ShowDialog = Windows.Forms.DialogResult.OK Then
                commRemotePlayer = windowConnectToOpponent.commRemotePlayer

                ' send this player's name.
                commRemotePlayer.SendData(newGameSettings.txtPlayer1Name.Text)

                ' Read opponents name and game piece.
                Dim dataFromClient As String = ""
                Do While dataFromClient = ""
                    dataFromClient = commRemotePlayer.ReceiveData()
                Loop
                dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"))

                'The X player usually goes first, so we'll assign X to whichever player goes first and O to the other.
                Dim strPlayerName As String = dataFromClient.Split("|")(0)
                Dim strPlayerPiece As String = dataFromClient.Split("|")(1)
                If strPlayerPiece = classPlayer.GamePiece.O.ToString Then
                    playerOne = New classPlayer(newGameSettings.txtPlayer1Name.Text, newGameSettings.comboPlayer1Type.SelectedIndex, classPlayer.GamePiece.X)
                    playerTwo = New classPlayer(strPlayerName, newGameSettings.comboPlayer2Type.SelectedIndex, classPlayer.GamePiece.O)
                    playerCurrent = playerOne
                    playerNext = playerTwo
                Else
                    playerOne = New classPlayer(newGameSettings.txtPlayer1Name.Text, newGameSettings.comboPlayer1Type.SelectedIndex, classPlayer.GamePiece.O)
                    playerTwo = New classPlayer(strPlayerName, newGameSettings.comboPlayer2Type.SelectedIndex, classPlayer.GamePiece.X)
                    playerCurrent = playerTwo
                    playerNext = playerOne
                End If

            Else ' if FormNetworkConnection was canceled.
                Return False
            End If
        Else ' if formNewGame was canceled.
            Return False
        End If

        ' Everything was successful, return true.
        Return True
    End Function ' END NewGame

    ''' <summary>
    ''' Play again.  NewGame() must be called prior to using this function.
    ''' </summary>
    ''' <param name="playerOne">The first player's classPlayer object.</param>
    ''' <param name="playerTwo">The second player's classPlayer object.</param>
    Public Sub StartGame(ByRef playerOne As classPlayer, ByRef playerTwo As classPlayer)
        ' Randomly pick which player will go first and set up playerCurrent and playerNext accordingly.
        '  Note: This is used to play additional games using the same parameters established in NewGame().
        '        NewGame() must be used to start the first game.
        Dim rndCoin As New Random(Now.Millisecond)
        If rndCoin.Next(1, 3) = 1 Then
            playerCurrent = playerOne
            playerNext = playerTwo
        Else
            playerCurrent = playerTwo
            playerNext = playerOne
        End If
    End Sub ' END StartGame
End Class ' END classGame
