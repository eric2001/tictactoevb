﻿' Project Name:  TicTacToeVB
' Copyright 2011 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 02 October 2011

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formWaitForConnection
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(formWaitForConnection))
        Me.lblWaiting = New System.Windows.Forms.Label()
        Me.lblNetworkAdd = New System.Windows.Forms.Label()
        Me.lblInternetAdd = New System.Windows.Forms.Label()
        Me.txtInternetAddress = New System.Windows.Forms.TextBox()
        Me.txtNetworkAddress = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'lblWaiting
        '
        Me.lblWaiting.AutoSize = True
        Me.lblWaiting.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWaiting.Location = New System.Drawing.Point(10, 12)
        Me.lblWaiting.Name = "lblWaiting"
        Me.lblWaiting.Size = New System.Drawing.Size(254, 20)
        Me.lblWaiting.TabIndex = 0
        Me.lblWaiting.Text = "Waiting for Opponent to Connect..."
        '
        'lblNetworkAdd
        '
        Me.lblNetworkAdd.AutoSize = True
        Me.lblNetworkAdd.Location = New System.Drawing.Point(37, 45)
        Me.lblNetworkAdd.Name = "lblNetworkAdd"
        Me.lblNetworkAdd.Size = New System.Drawing.Size(81, 13)
        Me.lblNetworkAdd.TabIndex = 1
        Me.lblNetworkAdd.Text = "Network Name:"
        '
        'lblInternetAdd
        '
        Me.lblInternetAdd.AutoSize = True
        Me.lblInternetAdd.Location = New System.Drawing.Point(37, 71)
        Me.lblInternetAdd.Name = "lblInternetAdd"
        Me.lblInternetAdd.Size = New System.Drawing.Size(87, 13)
        Me.lblInternetAdd.TabIndex = 2
        Me.lblInternetAdd.Text = "Internet Address:"
        '
        'txtInternetAddress
        '
        Me.txtInternetAddress.Location = New System.Drawing.Point(138, 68)
        Me.txtInternetAddress.Name = "txtInternetAddress"
        Me.txtInternetAddress.Size = New System.Drawing.Size(100, 20)
        Me.txtInternetAddress.TabIndex = 3
        '
        'txtNetworkAddress
        '
        Me.txtNetworkAddress.Location = New System.Drawing.Point(138, 42)
        Me.txtNetworkAddress.Name = "txtNetworkAddress"
        Me.txtNetworkAddress.Size = New System.Drawing.Size(100, 20)
        Me.txtNetworkAddress.TabIndex = 4
        '
        'formWaitForConnection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(275, 101)
        Me.Controls.Add(Me.txtNetworkAddress)
        Me.Controls.Add(Me.txtInternetAddress)
        Me.Controls.Add(Me.lblInternetAdd)
        Me.Controls.Add(Me.lblNetworkAdd)
        Me.Controls.Add(Me.lblWaiting)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "formWaitForConnection"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Connecting..."
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblWaiting As System.Windows.Forms.Label
    Friend WithEvents lblNetworkAdd As System.Windows.Forms.Label
    Friend WithEvents lblInternetAdd As System.Windows.Forms.Label
    Friend WithEvents txtInternetAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtNetworkAddress As System.Windows.Forms.TextBox
End Class
