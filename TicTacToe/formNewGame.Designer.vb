﻿' Project Name:  TicTacToeVB
' Copyright 2011 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 02 October 2011

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formNewGame
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(formNewGame))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.OK_Button = New System.Windows.Forms.Button()
        Me.Cancel_Button = New System.Windows.Forms.Button()
        Me.lblPlayer1 = New System.Windows.Forms.Label()
        Me.lblPlayer2 = New System.Windows.Forms.Label()
        Me.comboPlayer1Type = New System.Windows.Forms.ComboBox()
        Me.comboPlayer2Type = New System.Windows.Forms.ComboBox()
        Me.txtPlayer1Name = New System.Windows.Forms.TextBox()
        Me.txtPlayer2Name = New System.Windows.Forms.TextBox()
        Me.buttonClient = New System.Windows.Forms.Button()
        Me.buttonServer = New System.Windows.Forms.Button()
        Me.panelOnlineControls = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.panelOnlineControls.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.OK_Button, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Cancel_Button, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(263, 67)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(146, 29)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'OK_Button
        '
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(67, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "OK"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(76, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(67, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Cancel"
        '
        'lblPlayer1
        '
        Me.lblPlayer1.AutoSize = True
        Me.lblPlayer1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPlayer1.Location = New System.Drawing.Point(12, 9)
        Me.lblPlayer1.Name = "lblPlayer1"
        Me.lblPlayer1.Size = New System.Drawing.Size(65, 18)
        Me.lblPlayer1.TabIndex = 0
        Me.lblPlayer1.Text = "Player 1:"
        '
        'lblPlayer2
        '
        Me.lblPlayer2.AutoSize = True
        Me.lblPlayer2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPlayer2.Location = New System.Drawing.Point(12, 43)
        Me.lblPlayer2.Name = "lblPlayer2"
        Me.lblPlayer2.Size = New System.Drawing.Size(65, 18)
        Me.lblPlayer2.TabIndex = 3
        Me.lblPlayer2.Text = "Player 2:"
        '
        'comboPlayer1Type
        '
        Me.comboPlayer1Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboPlayer1Type.FormattingEnabled = True
        Me.comboPlayer1Type.Items.AddRange(New Object() {"Human Player", "Computer (Easy)", "Computer (Hard)"})
        Me.comboPlayer1Type.Location = New System.Drawing.Point(83, 6)
        Me.comboPlayer1Type.Name = "comboPlayer1Type"
        Me.comboPlayer1Type.Size = New System.Drawing.Size(121, 21)
        Me.comboPlayer1Type.TabIndex = 1
        '
        'comboPlayer2Type
        '
        Me.comboPlayer2Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comboPlayer2Type.FormattingEnabled = True
        Me.comboPlayer2Type.Items.AddRange(New Object() {"Human Player", "Computer (Easy)", "Computer (Hard)", "Remote Player"})
        Me.comboPlayer2Type.Location = New System.Drawing.Point(83, 40)
        Me.comboPlayer2Type.Name = "comboPlayer2Type"
        Me.comboPlayer2Type.Size = New System.Drawing.Size(121, 21)
        Me.comboPlayer2Type.TabIndex = 4
        '
        'txtPlayer1Name
        '
        Me.txtPlayer1Name.Location = New System.Drawing.Point(223, 7)
        Me.txtPlayer1Name.Name = "txtPlayer1Name"
        Me.txtPlayer1Name.Size = New System.Drawing.Size(197, 20)
        Me.txtPlayer1Name.TabIndex = 2
        Me.txtPlayer1Name.Text = "Player 1"
        '
        'txtPlayer2Name
        '
        Me.txtPlayer2Name.Location = New System.Drawing.Point(223, 41)
        Me.txtPlayer2Name.Name = "txtPlayer2Name"
        Me.txtPlayer2Name.Size = New System.Drawing.Size(197, 20)
        Me.txtPlayer2Name.TabIndex = 5
        Me.txtPlayer2Name.Text = "Player 2"
        '
        'buttonClient
        '
        Me.buttonClient.Location = New System.Drawing.Point(3, 3)
        Me.buttonClient.Name = "buttonClient"
        Me.buttonClient.Size = New System.Drawing.Size(100, 23)
        Me.buttonClient.TabIndex = 6
        Me.buttonClient.Text = "Connect to Game"
        Me.buttonClient.UseVisualStyleBackColor = True
        '
        'buttonServer
        '
        Me.buttonServer.Location = New System.Drawing.Point(110, 3)
        Me.buttonServer.Name = "buttonServer"
        Me.buttonServer.Size = New System.Drawing.Size(100, 23)
        Me.buttonServer.TabIndex = 7
        Me.buttonServer.Text = "Start New Game"
        Me.buttonServer.UseVisualStyleBackColor = True
        '
        'panelOnlineControls
        '
        Me.panelOnlineControls.Controls.Add(Me.buttonClient)
        Me.panelOnlineControls.Controls.Add(Me.buttonServer)
        Me.panelOnlineControls.Location = New System.Drawing.Point(123, 67)
        Me.panelOnlineControls.Name = "panelOnlineControls"
        Me.panelOnlineControls.Size = New System.Drawing.Size(213, 29)
        Me.panelOnlineControls.TabIndex = 8
        Me.panelOnlineControls.Visible = False
        '
        'formNewGame
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(435, 101)
        Me.Controls.Add(Me.panelOnlineControls)
        Me.Controls.Add(Me.txtPlayer2Name)
        Me.Controls.Add(Me.txtPlayer1Name)
        Me.Controls.Add(Me.comboPlayer2Type)
        Me.Controls.Add(Me.comboPlayer1Type)
        Me.Controls.Add(Me.lblPlayer2)
        Me.Controls.Add(Me.lblPlayer1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "formNewGame"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "New Game..."
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.panelOnlineControls.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents OK_Button As System.Windows.Forms.Button
    Friend WithEvents Cancel_Button As System.Windows.Forms.Button
    Friend WithEvents lblPlayer1 As System.Windows.Forms.Label
    Friend WithEvents lblPlayer2 As System.Windows.Forms.Label
    Friend WithEvents comboPlayer1Type As System.Windows.Forms.ComboBox
    Friend WithEvents comboPlayer2Type As System.Windows.Forms.ComboBox
    Friend WithEvents txtPlayer1Name As System.Windows.Forms.TextBox
    Friend WithEvents txtPlayer2Name As System.Windows.Forms.TextBox
    Friend WithEvents buttonClient As System.Windows.Forms.Button
    Friend WithEvents buttonServer As System.Windows.Forms.Button
    Friend WithEvents panelOnlineControls As System.Windows.Forms.Panel

End Class
