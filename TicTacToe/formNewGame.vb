﻿' Project Name:  TicTacToeVB
' Copyright 2011 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 02 October 2011

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Imports System.Windows.Forms

Public Class formNewGame

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        ' Force the user to provide unique names for Player 1 and Player 2.
        If txtPlayer1Name.Text = "" Or txtPlayer2Name.Text = "" Then
            MessageBox.Show("Please provide names for both players.", "Tic Tac Toe", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        ElseIf txtPlayer1Name.Text = txtPlayer2Name.Text Then
            MessageBox.Show("Please provide different names for both players.", "Tic Tac Toe", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Me.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Close()
        End If
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        ' If the cancel button is pressed, return Cancel and close the window.
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub formNewGame_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Set the default for both players to Human.
        comboPlayer1Type.SelectedIndex = 0
        comboPlayer2Type.SelectedIndex = 0
    End Sub

    Private Sub comboPlayer2Type_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comboPlayer2Type.SelectedIndexChanged
        ' Change the button options if the second player is an internet player.
        '   Also, disable player 2's name for internet games (this will be specified by player 2 instead).
        If comboPlayer2Type.SelectedIndex = classPlayer.PlayerAI.Remote Then
            txtPlayer2Name.Enabled = False
            panelOnlineControls.Visible = True
            Me.AcceptButton = buttonClient
        Else
            txtPlayer2Name.Enabled = True
            panelOnlineControls.Visible = False
            Me.AcceptButton = OK_Button
        End If
    End Sub

    Private Sub buttonClient_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonClient.Click
        ' Force the user to provide a user name for Player 1, then return No to indicate 
        '   That the user want's to connect to an existing game and close the window.
        If txtPlayer1Name.Text = "" Then
            MessageBox.Show("Please provide a name for Player 1.", "Tic Tac Toe", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Me.DialogResult = System.Windows.Forms.DialogResult.No
            Me.Close()
        End If
    End Sub

    Private Sub buttonServer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonServer.Click
        ' Force the user to provide a user name for Player 1, then return Yes to indicate 
        '   That the user want's to start a new online game and close this window.
        If txtPlayer1Name.Text = "" Then
            MessageBox.Show("Please provide a name for Player 1.", "Tic Tac Toe", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Me.DialogResult = System.Windows.Forms.DialogResult.Yes
            Me.Close()
        End If
    End Sub
End Class
