﻿' Project Name:  TicTacToeVB
' Copyright 2011 Eric Cavaliere
' License:  GPL Version 3
' Project Start Date: 02 October 2011

' This program is free software: you can redistribute it and/or modify
' it under the terms of the GNU General Public License as published by
' the Free Software Foundation, either version 3 of the License, or
' (at your option) any later version.

' This program is distributed in the hope that it will be useful,
' but WITHOUT ANY WARRANTY; without even the implied warranty of
' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
' GNU General Public License for more details.

' You should have received a copy of the GNU General Public License
' along with this program.  If not, see <http://www.gnu.org/licenses/>.

Imports System.Net.Sockets

Public Class formWaitForConnection
    Public Function WaitForPlayer(ByRef commRemotePlayer As classCommunication) As Boolean
        ' Wait for another computer to connect to this one.
        '  We're doing this in a seperate function to ensure that this window
        '  has time to properly load before the WaitForConnection function locks it down.
        Dim result As Boolean = commRemotePlayer.WaitForConnection()
        Me.Close()
        Return result
    End Function

    Private Sub formWaitForConnection_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Get the local network name and the internet ip address for this computer
        '   and display it on the window.  This should help let the user know their 
        '   address so they can inform their opponent where to connect to.
        ' We're using a web page to determine the computer's IP address as there's no other
        '   reliable way to accurately figure it out across the various types of networks
        '   and internet services.
        txtNetworkAddress.Text = System.Net.Dns.GetHostName() ' Get local network name.
        txtInternetAddress.Text = HTTPDownload("http://www.cavaliere.me/checkip.asp") ' get ip address.
    End Sub

    Function HTTPDownload(ByVal url As String) As String
        ' Download a web page and return the result.  On error return "".
        Dim request As System.Net.WebRequest
        Dim response As System.Net.HttpWebResponse

        ' Make sure url is not null
        If url.Length = 0 Then
            Return ""
        End If

        ' Connect to remote web server, on error exit.
        request = System.Net.WebRequest.Create(url)
        Try
            response = CType(request.GetResponse(), System.Net.HttpWebResponse)
            request.Timeout = 90 * 1000 ' seconds * 1000
            request.Credentials = System.Net.CredentialCache.DefaultCredentials
        Catch ex As Exception
            Return ""
        End Try

        ' Download the file, on error exit.
        If response.StatusDescription.ToString = "OK" Then
            Dim responseFromServer As String
            Dim dataStream As System.IO.Stream
            Dim reader As System.IO.StreamReader
            Try
                dataStream = response.GetResponseStream()
                reader = New System.IO.StreamReader(dataStream)
                responseFromServer = reader.ReadToEnd()
                reader.Close()
                dataStream.Close()
            Catch ex As Exception
                Return ""
            End Try

            Return responseFromServer
        Else
            Return ""
        End If
    End Function
End Class